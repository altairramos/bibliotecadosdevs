﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDD_BibliotecaDosDevs {
    public class MonthRevenue {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Revenue { get; set; }
    }
}
