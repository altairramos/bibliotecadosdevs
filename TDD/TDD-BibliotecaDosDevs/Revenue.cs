﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDD_BibliotecaDosDevs {
    public static class Revenue {
        public static List<MonthRevenue> GetMonthRevenues(MonthRevenue monthRevenue) {
            if (monthRevenue == null) {
                throw  new Exception("Parameter is null!");
            }

            var monthRevenuesList = new List<MonthRevenue>();

            int months = ((monthRevenue.EndDate.Year - monthRevenue.StartDate.Year) * 12) + (monthRevenue.EndDate.Month - monthRevenue.StartDate.Month);

            for (int i = 0; i < months; i++) {
                monthRevenuesList.Add(new MonthRevenue() {
                    StartDate = monthRevenue.StartDate.AddMonths(i),
                    EndDate = monthRevenue.StartDate.AddMonths(i + 1),
                    Revenue = 0
                });
            }

            return monthRevenuesList;
        }
    }
}
