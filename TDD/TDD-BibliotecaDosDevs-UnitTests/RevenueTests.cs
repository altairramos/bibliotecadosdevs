using System;
using System.Linq;
using TDD_BibliotecaDosDevs;
using Xunit;

namespace TDD_BibliotecaDosDevs_UnitTests {
    public class RevenueTests {
        [Fact]
        public void ListMonths_should_return_list_of_objects_when_paramter_is_valid()
        {
            // Arrange
            MonthRevenue monthRevenue = new MonthRevenue {
                StartDate = new DateTime(2020, 01, 01),
                EndDate = new DateTime(2020, 05, 01)
            };

            // Act
            var monthRevenues = Revenue.GetMonthRevenues(monthRevenue);

            // Assert
            Assert.NotNull(monthRevenues);
        }

        [Fact]
        public void ListMonths_should_return_two_items_when_months_range_are_two()
        {
            // Arrange
            MonthRevenue monthRevenue = new MonthRevenue {
                StartDate = new DateTime(2020, 03, 01),
                EndDate = new DateTime(2020, 05, 01)
            };

            // Act
            var monthRevenues = Revenue.GetMonthRevenues(monthRevenue);

            // Assert
            Assert.Equal(2, monthRevenues.Count());
        }

        [Fact]
        public void ListMonths_should_thown_exception_when_parameter_is_null()
        {
            // Arrange
            MonthRevenue monthRevenue = new MonthRevenue {
                StartDate = new DateTime(2020, 01, 01),
                EndDate = new DateTime(2020, 05, 01)
            };

            // Act and Assert
            Assert.Throws<Exception>(() => Revenue.GetMonthRevenues(null));
        }
    }
}
